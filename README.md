# Randonneurs Sverige Kalendar

This is a web application for displaying and searching through a list of
brevet rides occuring in Sweden.

A live version is currently available here:
[Randonneurs Sverige Kalendar 2022](https://randonneuring.gitlab.io/randonneurs-sverige-kalendar/)

The source for the brevet schedule itself has been taken from the official
Randonneurs Sverige website:
[Randonneurs Sverige](http://www.randonneurs.se/)

Screenshot:
![screenshot](screenshots/2021-10-29_screenshot.png)
