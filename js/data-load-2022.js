/*global $*/
'use strict';


// External libraries
var DATA_DISPLAY,

    // Global variables
    DATA_LOAD =
        {
            // The relative location of the data files
            dataSource : {
                "2022" : "../data/randonneurs-sverige-calendar-2022.json"
            },

            // The dictionary array into which all loaded data will be placed
            brevetObjectArray : {},
            response : [],

            // Send an ajax request
            ajaxRequest : function (url, dataType) {

                var debug = true;

                // Send an ajax request to the server - the return value is the
                // response
                return $.ajax({

                    // Add version number url
                    url: url + "?v=202110271541",

                    success: function (response) {
                        if (debug) {
                            console.debug("AJAX " + url + " request success");
                            console.debug(response);
                        }
                    },

                    error: function (x, status, error) {
                        // Unauthorized access error
                        if (x.status === 403) {
                            console.log(
                                'HTTP 403: Forbidden (Access is not permitted)'
                            );
                        // Other errors
                        } else {
                            console.log("An error occurred: " + status);
                            console.log("Error: " + error);
                        }
                    },

                    // Settings used in all ajax requests
                    type: "GET",
                    dataType: dataType,
                    async: true,
                    cache: true,
                    timeout: 5000
                });

            },


            // The id of the table needs to already exist in the html file
            // The column names need to match what is in the data files
            createTable : function (data) {
                $('#calendar_table').DataTable(
                    {
                        "data": data,
                        "columns": [
                            { "data": "Location" },
                            { "data": "Date" },
                            { "data": "Distance" },
                            { "data": "Organizer" },
                            { "data": "Phone" },
                            { "data": "Club" },
                        ],
                        "paging": false,
                        "select": true,
                        "dom": 'Bfrtip',
                        "fixedHeader": {
                            "header": true,
                            "footer": true
                        }
                    }
                ).columns.adjust();
                // The "columns.adjust()" is executed inorder to force a
                // re-draw of the table, otherwise the table does not properly
                // fill the available space properly during a resize event
            },


            // Load the data from given json files
            loadBrevetData : function () {

                var debug = true, dataFile, requests = [];

                if (debug) {
                    console.log("loadBrevetData called");
                }

                // Loop over each year and read the data file associated with
                // it
                Object.keys(DATA_LOAD.dataSource).forEach(
                    function (year, index) {

                        // Get the data file name
                        dataFile = DATA_LOAD.dataSource[year];

                        if (debug) {
                            console.log("year: ", year, ", index: ", index);
                            console.log("dataFile: ", dataFile);
                        }

                        // Read json data, run as ajax request.
                        requests.push(
                            $.when(DATA_LOAD.ajaxRequest(dataFile, "json")
                                    ).then(

                                function (response) {
                                    // Save output to a global variable
                                    DATA_LOAD.response[year] = response;
                                }
                            )
                        );

                    }
                );

                // After data reading is done, send to table creation function
                $.when.apply($, requests).then(
                    function () {
                        var i, j, brevets,
                            organizer, email, mailto,
                            phone, club, clubLink,
                            date, url, distance,
                            dateLink, brevetLocation, brevetObject = {},
                            brevetObjectArray = [];

                        if (debug) {
                            console.log(DATA_LOAD.response["2022"][0]);
                        }

                        // Loop over all data elements
                        for (i = 0; i < DATA_LOAD.response["2022"].length;
                                i += 1) {

                            // Get information about this start location
                            brevetLocation =
                                DATA_LOAD.response["2022"][i].Location;
                            organizer =
                                DATA_LOAD.response["2022"][i].Organizer;
                            email = DATA_LOAD.response["2022"][i].Email;
                            phone = DATA_LOAD.response["2022"][i].Phone;
                            club = DATA_LOAD.response["2022"][i].Club;
                            url = DATA_LOAD.response["2022"][i].Url;
                            brevets = DATA_LOAD.response["2022"][i].Brevets;

                            // Do not continue if data provided is not
                            // adequate
                            if (brevetLocation !== "") {

                                // Create email mailto link from email and
                                // organizer
                                mailto = '<a target="_blank" rel="noreferrer"'
                                    + ' href="mailto:' + email + '">' +
                                    organizer + '</a>';

                                // Create html link from Url and Club
                                clubLink = '<a target="_blank" ' +
                                    'rel="noreferrer"' + ' href="' + url +
                                    '">' + club + '</a>';

                                if (debug) {
                                    if (i < 5) {
                                        console.log(organizer);
                                        console.log(email);
                                        console.log(mailto);
                                        console.log(clubLink);
                                    }
                                    console.log("Number of brevets:",
                                        brevets.length);
                                }

                                // Loop over all brevets
                                for (j = 0; j < brevets.length; j += 1) {

                                    if (debug) {
                                        console.log(brevets[j]);
                                    }

                                    // Get information about this brevet
                                    date = brevets[j].Date;
                                    url = brevets[j].Url;
                                    distance = brevets[j].Distance;

                                    // Create an html link from Url and Date
                                    if (url !== "") {
                                        dateLink = '<a target="_blank"' +
                                            'rel="noreferrer"' + ' href="' +
                                            url + '">' + date + '</a>';
                                    } else {
                                        dateLink = date;
                                    }

                                    brevetObject = {
                                        "Date" : dateLink,
                                        "Distance" : distance,
                                        "Location" : brevetLocation,
                                        "Organizer" : mailto,
                                        "Phone" : phone,
                                        "Club" : clubLink,
                                    };

                                    if (debug) {
                                        console.log(brevetObject);
                                    }

                                    // Add the brevet object to the brevet
                                    // object array
                                    brevetObjectArray.push(brevetObject);
                                }
                            }

                        }

                        // Create the table
                        DATA_LOAD.createTable(brevetObjectArray);
                    }
                );

            },

        };


// This function fires when the page is ready
$(document).ready(function () {

    var debug = true;

    if (debug) {
        console.log('document is ready');
    }

    // Load all the data and plot it
    DATA_LOAD.loadBrevetData();

});
