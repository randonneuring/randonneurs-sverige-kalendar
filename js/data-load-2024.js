/*global $*/
'use strict';


// External libraries
var DATA_LOAD;

// This function fires when the page is ready
$(document).ready(function () {

    var debug = true;

    if (debug) {
        console.log('document is ready');
    }

    // Load all the data and plot it
    DATA_LOAD.loadBrevetData('2024');

});
